$(document).ready (function() {
    $('#select').change(function() {
        window.location.href = "/home/refresh?size="+ $('#select').val() +"&page=1";
    });

    $('#radioBtn a').on('click', function(){
            var sel = $(this).data('title');
            var tog = $(this).data('toggle');
            $('#'+tog).prop('value', sel);
            $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
            $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
        });
});

function setForm(element) {
    var button_id = element.value;
    $('#pid').text(button_id);
    $('#find').attr("value", button_id);
    var title_val = $('#name'+button_id).html();
    var author_val = $('#author'+button_id).html();
    var isbn_val = $('#isbn'+button_id).html();
    $('#name').val(title_val);
    $('#author').val(author_val);
    $('#isbn').val(isbn_val);
}

function saveChanges() {
    var book_id = document.forms["form"].elements["find"].value;
    var book_title = document.forms["form"].elements["name"].value;
    var book_author = document.forms["form"].elements["author"].value;
    var book_isbn = document.forms["form"].elements["isbn"].value;

  $.ajax({
    type: 'GET',
    url: '/editBook',
    data: {
      'id': book_id,
      'name': book_title,
      'author': book_author,
      'isbn': book_isbn
      }
    }).done(function (answer) {
        if (answer == true) {
          location.href = "/home/refresh";
        }
      }
    )
}

function deleteBook(element) {
    var button_id = element.value;
    $.ajax({
        type: 'GET',
        url: '/deleteBook',
        data: {
          'id': button_id,
          }
        }).done(function (answer) {
            if (answer == true) {
              element.closest('tr').remove();
            }
          }
        )
}


