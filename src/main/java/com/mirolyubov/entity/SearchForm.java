package com.mirolyubov.entity;

import lombok.Data;

@Data
public class SearchForm {

    private String line;
    private int parameter;
}
