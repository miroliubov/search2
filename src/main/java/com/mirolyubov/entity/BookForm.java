package com.mirolyubov.entity;

import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class BookForm {

    private int id;
    @NotEmpty(message = "Input author")
    private String author;
    @NotEmpty(message = "Input title")
    private String name;
    private int isbn;

}
