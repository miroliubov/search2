package com.mirolyubov.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class LocalisationController {

    @GetMapping("/lang")
    public String changeLanguage(HttpServletRequest request) {
        return "redirect:" + request.getHeader("referer");
    }
}
