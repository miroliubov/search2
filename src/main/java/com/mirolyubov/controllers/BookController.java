package com.mirolyubov.controllers;

import com.mirolyubov.entity.Book;
import com.mirolyubov.entity.BookForm;
import com.mirolyubov.entity.SearchForm;
import com.mirolyubov.services.IBookService;
import com.mirolyubov.services.IJpaBookService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class BookController {

    @Autowired
    private IBookService bookService;

    @Autowired
    private IJpaBookService jpaBookService;

    private static Logger logger = Logger.getLogger(BookController.class);

    @GetMapping("/showBooks")
    public ModelAndView findCities(Model model) {

        ModelAndView modelAndView = new ModelAndView();
        List<Book> books = new ArrayList<>();
        books = jpaBookService.findAll();

        modelAndView.addObject("books", books);
        modelAndView.setViewName("books");
        return modelAndView;
    }

    @RequestMapping(value = {"/addBook"}, method = RequestMethod.POST)
    public ModelAndView savePerson(Model model, @Valid @ModelAttribute("bookForm") BookForm bookForm, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        if (bindingResult.hasErrors()) {
            List<ObjectError> allErrors = bindingResult.getAllErrors();
            modelAndView.addObject("errors", allErrors);
            modelAndView.setViewName("403");
            return modelAndView;
        } else {
            bookService.addBook(bookForm);
            modelAndView.setViewName("redirect:/home/refresh");
            logger.info("Book added: " + bookForm);
            return modelAndView;
        }
    }

    @GetMapping("/home/refresh")
    public String home(Locale locale, Model model, @RequestParam("page") Optional<Integer> page,
                       @RequestParam("size") Optional<Integer> size) {

        Page<Book> bookPage = bookService.getBookPage(page.orElse(1), size.orElse(5));
        model.addAttribute("bookPage", bookPage);

        int totalPages = bookPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }

        SearchForm searchForm = new SearchForm();
        searchForm.setParameter(1);
        model.addAttribute("bookForm", new BookForm());
        model.addAttribute("searchForm", searchForm);

        return "home";
    }

    @RequestMapping(value = { "/searchBook" }, method = RequestMethod.POST)
    public String searchBookn(Model model, @ModelAttribute("searchForm") SearchForm searchForm) {
        String resultLine = searchForm.getLine();
        int searchParameter = searchForm.getParameter();

        List<Book> bookList = bookService.findBooksByAttributes(resultLine, searchParameter);
        model.addAttribute("bookList", bookList);
        return "searchresults";
    }

    @GetMapping("/editBook")
    @ResponseBody
    public boolean editBook(Book book) {
        return bookService.editFilm(book);
    }

    @GetMapping("/deleteBook")
    @ResponseBody
    public boolean deleteBook(long id) {
        return bookService.deleteBook(id);
    }

//    @GetMapping("/error")
//    public String error() {
//        return "/403";
//    }
}