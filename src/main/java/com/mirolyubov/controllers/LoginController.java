package com.mirolyubov.controllers;

import com.mirolyubov.dto.BookRepository;
import com.mirolyubov.dto.IBookRepository;
import com.mirolyubov.entity.Book;
import com.mirolyubov.entity.BookForm;
import com.mirolyubov.entity.SearchForm;
import com.mirolyubov.services.IBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class LoginController {

    @Autowired
    IBookService bookService;

    @GetMapping(value = {"/", "/index"})
    public String index() {
        return "redirect:/about";
    }

    @GetMapping("/about")
    public String about() {
        return "about";
    }

    @GetMapping("/login")
    public ModelAndView login(Model model) {
        ModelAndView modelAndView = new ModelAndView();

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (!(auth instanceof AnonymousAuthenticationToken)) {
            return new ModelAndView("redirect:/about");
        }

        modelAndView.setViewName("login");
        return modelAndView;
    }

    @GetMapping("/error")
    public String error403() {
        return "403";
    }


}
