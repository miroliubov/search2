package com.mirolyubov.dto;

import com.mirolyubov.entity.Book;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class BookRepository implements IBookRepository {
    List<Book> listOfBooks = new ArrayList<>();

    {
        listOfBooks.add(new Book(1, "Karl Marx", "Capital", 6671));
        listOfBooks.add(new Book(2, "Jules Verne", "Twenty Thousand Leagues Under the Sea", 228));
        listOfBooks.add(new Book(3, "Jules Verne", "Around the World in Eighty Days", 123));
        listOfBooks.add(new Book(4, "Jules Verne", "Journey to the Center of the Earth", 222));
        listOfBooks.add(new Book(5, "Alexandre Dumas", "The Three Musketeers", 1337));
        listOfBooks.add(new Book(6, "Alexandre Dumas", "Twenty Years After", 455));
        listOfBooks.add(new Book(7, "Alexandre Dumas", "The Count of Monte Cristo", 6654));
        listOfBooks.add(new Book(8, "Alexandre Dumas", "Queen Margot", 7821));
        listOfBooks.add(new Book(9, "George Orwell", "1984 Nineteen Eighty-Four", 1337));
        listOfBooks.add(new Book(10, "George Orwell", "Animal Farm: A Fairy Story", 7942));
        listOfBooks.add(new Book(11, "Aleksandr Solzhenitsyn", "The Gulag Archipelago", 3480));
        listOfBooks.add(new Book(12, "Aleksandr Solzhenitsyn", "Matryona's Place", 1788));
        listOfBooks.add(new Book(13, "Fyodor Dostoevsky", "Crime and Punishment ", 645213));
        listOfBooks.add(new Book(14, "Fyodor Dostoevsky", "The Idiot", 4562));
        listOfBooks.add(new Book(15, "Fyodor Dostoevsky", "Demons", 2565));
        listOfBooks.add(new Book(16, "Anton Chekhov", "The Death of a Government Clerk", 7832));
    }

    @Override
    public List<Book> getListOfBooks() {
        return listOfBooks;
    }

    @Override
    public void addBook(Book book) {
        listOfBooks.add(book);
    }

    @Override
    public List<Book> getBooksByTitle(String resultLine) {
        List<Book> bookList = new ArrayList<>();
        for (Book book: listOfBooks) {
            if (book.getName().toLowerCase().contains(resultLine.toLowerCase())) {
                bookList.add(book);
            }
        }
        return bookList;
    }

    @Override
    public List<Book> getBooksByAuthor(String resultLine) {
        List<Book> bookList = new ArrayList<>();
        for (Book book: listOfBooks) {
            if (book.getAuthor().toLowerCase().contains(resultLine.toLowerCase())) {
                bookList.add(book);
            }
        }
        return bookList;
    }

    @Override
    public boolean editBook(Book book) {
        for (Book elem: listOfBooks) {
            if (elem.getId() == book.getId()) {
                elem.setName(book.getName());
                elem.setAuthor(book.getAuthor());
                elem.setIsbn(book.getIsbn());
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean deleteBook(long id) {
        for (Book elem: listOfBooks) {
            if (elem.getId() == id) {
                listOfBooks.remove(elem);
                return true;
            }
        }
        return false;
    }
}
