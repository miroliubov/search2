package com.mirolyubov.dto;

import com.mirolyubov.entity.Book;

import java.util.List;

public interface IBookRepository {
    List<Book> getListOfBooks();

    void addBook(Book book);

    List<Book> getBooksByTitle(String resultLine);

    List<Book> getBooksByAuthor(String resultLine);

    boolean editBook(Book book);

    boolean deleteBook(long id);
}