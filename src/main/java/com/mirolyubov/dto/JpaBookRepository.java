package com.mirolyubov.dto;

import com.mirolyubov.entity.Book;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JpaBookRepository extends CrudRepository<Book, Long> {
}
