package com.mirolyubov.services;

import com.mirolyubov.entity.Book;

import java.util.List;

public interface IJpaBookService {
    List<Book> findAll();
}
