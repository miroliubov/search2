package com.mirolyubov.services;

import com.mirolyubov.entity.Book;
import com.mirolyubov.entity.BookForm;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


import java.util.List;

public interface IBookService {
    List<Book> findBooksByAttributes(String resultLine, int searchParameter);

    Page<Book> findPaginated(Pageable pageable);

    int calculateAmountOfPages(Integer size);

    Page<Book> getBookPage(int currentPage, int pageSize);

    boolean editFilm(Book book);

    boolean deleteBook(long id);

    void addBook(BookForm bookForm);
}
