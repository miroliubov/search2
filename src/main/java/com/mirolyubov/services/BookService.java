package com.mirolyubov.services;

import com.mirolyubov.dto.IBookRepository;
import com.mirolyubov.entity.Book;
import com.mirolyubov.entity.BookForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


import java.util.Collections;
import java.util.List;

@Service
public class BookService implements IBookService {

    @Autowired
    IBookRepository bookRepository;

    @Override
    public List<Book> findBooksByAttributes(String resultLine, int searchParameter) {
        if (searchParameter == 1) {
            List<Book> bookListByBookTitle = bookRepository.getBooksByTitle(resultLine);
            return bookListByBookTitle;
        } else if (searchParameter == 2) {
            List<Book> bookListByBookAuthor = bookRepository.getBooksByAuthor(resultLine);
            return bookListByBookAuthor;
        } else {
            return null;
        }
    }

    @Override
    public Page<Book> findPaginated(Pageable pageable) {
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Book> list;
        List<Book> books = bookRepository.getListOfBooks();

        if (books.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, books.size());
            list = books.subList(startItem, toIndex);
        }

        Page<Book> bookPage
                = new PageImpl<Book>(list, new PageRequest(currentPage, pageSize), books.size());

        return bookPage;
    }

    @Override
    public int calculateAmountOfPages(Integer size) {
        int amount = bookRepository.getListOfBooks().size();
        return (int) Math.ceil(amount / new Double(size));
    }

    @Override
    public Page<Book> getBookPage(int currentPage, int pageSize) {
        Page<Book> bookPage = findPaginated(new PageRequest(currentPage - 1, pageSize));
        return bookPage;
    }

    @Override
    public boolean editFilm(Book book) {
        if (bookRepository.editBook(book)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean deleteBook(long id) {
        if (bookRepository.deleteBook(id)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void addBook(BookForm bookForm) {
        String name = bookForm.getName();
        String author = bookForm.getAuthor();
        int id = bookForm.getId();
        int isbn = bookForm.getIsbn();

        Book newBook = new Book(id, author, name, isbn);
        bookRepository.addBook(newBook);
    }
}
