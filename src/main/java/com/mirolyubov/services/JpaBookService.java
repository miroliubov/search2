package com.mirolyubov.services;

import com.mirolyubov.dto.JpaBookRepository;
import com.mirolyubov.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JpaBookService implements IJpaBookService {

    @Autowired
    JpaBookRepository bookRepository;

    @Override
    public List<Book> findAll() {

        List<Book> books = (List<Book>) bookRepository.findAll();

        return books;
    }
}
