INSERT INTO books(author, name, isbn) VALUES('Karl Marx', 'Capital', 6671);
INSERT INTO books(author, name, isbn) VALUES('Jules Verne', 'Twenty Thousand Leagues Under the Sea', 228);
INSERT INTO books(author, name, isbn) VALUES('Jules Verne', 'Around the World in Eighty Days', 123);
INSERT INTO books(author, name, isbn) VALUES('Alexandre Dumas', 'The Three Musketeers', 1337);
INSERT INTO books(author, name, isbn) VALUES('Fyodor Dostoevsky', 'Crime and Punishment', 645213);