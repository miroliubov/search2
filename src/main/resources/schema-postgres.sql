DROP TABLE IF EXISTS books;
CREATE TABLE books(id serial PRIMARY KEY, author VARCHAR(100), name VARCHAR(100), isbn integer);