package com.mirolyubov.controllers;

import com.mirolyubov.ApplicationLauncher;
import com.mirolyubov.entity.Book;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;
import java.util.Arrays;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

//@RunWith(SpringJUnit4ClassRunner.class)
//@WebAppConfiguration
//@SpringBootTest
//@AutoConfigureMockMvc
public class BookControllerTest {

//    @Autowired
//    private MockMvc mockMvc;
//
//    @Autowired
//    private WebApplicationContext context;
//
//    @Test
//    public void testWebContext() {
//        ServletContext servletContext = context.getServletContext();
//        assertNotNull(servletContext);
//        assertTrue(servletContext instanceof MockServletContext);
//        assertNotNull(context.getBean(LoginController.class));
//    }
//
//    @Test
//    @WithMockUser(authorities = "USER")
//    public void editBook() throws Exception {
//        mockMvc.perform(get("/home/refresh"))
//                .andExpect(status().isOk());
//
//    }
//
//    @Test
//    @WithMockUser(authorities = "USER")
//    public void deleteBook() throws Exception {
//        MvcResult result = mockMvc.perform(get("/deleteBook?id=1"))
//                .andExpect(status().isOk())
//                .andReturn();
//
//        String content = result.getResponse().getContentAsString();
//        assertEquals("true", content);
//    }
//
//    @Test
//    public void loginName() throws Exception {
//        mockMvc.perform(get("/login"))
//                .andExpect(view().name("/login"));
//    }
}